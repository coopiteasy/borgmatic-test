## steps

1. `sudo apt install borgbackup`
2. `loginctl enable-linger odoo12` (allow systemd user services without being
   logged in)
3. switch to odoo12
4. `export XDG_RUNTIME_DIR="/run/user/$(id -u)"`
   (<https://access.redhat.com/discussions/6029491>)
5. `pipx install borgmatic`, `pipx ensurepath`
6. `mkdir borg1 borg2`
7. `git clone git@gitlab.com:coopiteasy/borgmatic-test.git`
8. create symbolic links
9. `borgmatic init -e repokey`
   (<https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-mode-tldr>;
   keyfile is stored alongside repo, encrypted by configured passphrase)
10. `borgmatic create` (test -> works)
11. `systemctl --user daemon-reload`
12. `systemctl --user enable --now borgmatic.timer`

## notes

- borg is the backup software; borgmatic is the configuratory syntactic sugar
  that wraps around it.
- backups are sent to two repositories. these are ideally remote over ssh, but
  in this case they're in ~/borg[1,2]
- using a systemd timer, backup + prune + check is done every hour. the
  integrity check is expensive and could be run once per day instead.
- `borgmatic` does backup + prune + check for every database, but not in
  parallel, so this takes a long time. `all_borgmatic.py` was written to make
  this parallel, but it does _everything_ in parallel all at once. better load
  balancing would be nice.
- `borgmatic info` and `borgmatic list` are useful.
- `borgmatic -c ~/.config/borgmatic.d/[...].yaml` to only run a single config.
- `borgmatic -c [...] restore --archive latest` tries to restore the database.
  this doesn't work when using `plain` backups of the database. in any case this
  probably isn't what we want, because we want to trigger the SQL posthook using
  ociedoo.
- `borgmatic -c [...] extract --archive latest` puts the filestore back
  **in-place**.
- `borgmatic -c [...] extract --archive latest --destination /tmp/backup` puts
  the backed up filesystem hierarchy under `/tmp/backup`. unlike the previous
  command, the database dump appears in `/tmp/backup/~/.borgmatic`
- still need a way to securely store the encryption passphrase out-of-repo.
  could be as simple as putting it in a file in `$HOME`.
- creating the symbolic links should be automated/improved.
- figure out how to extract the backup to a different host (in our case: extract
  backup made by prod to test).
- create a script to autogenerate `borgmatic.d/[...].yaml` files because they're
  99% identical anyway.
- decide whether to have one common passphrase, or set a passphrase per
  repo/database.

---

example of `borgmatic -c [...] extract --destination /tmp/backup`:

```
/tmp/backup
└── home
    └── odoo12
        ├── .borgmatic
        │   └── postgresql_databases
        │       └── localhost
        │           └── coopcity-test
        └── .local
            └── share
                └── Odoo
                    └── filestore
                        └── coopcity-test
                            ├── 00
                            │   ├── 00151245ec5fe214517d9edda64e54797f815224
                            │   ├── 00da8b5ef6a89a2058dd21f586711a8d13696caa
                            │   └── 00fb4c413200cd65df61b6f2ad614d47b49451d5
                            │   [...]
                            └── ff
                                ├── ff4fe2a9ad4a0a960dae7a6e895b00c34f61e25a
                                └── ff98b7370bc86126f47410cb904eed6f1905f348
```

---

storage savings through compression + deduplication:

```
odoo12 $ borgmatic info
/home/odoo12/borg1/bees-test: Displaying summary info for archives
Repository ID: 7a42809ebe2bfe0dde03daafe5acc1c806170b9993dda2601cc529f941f109f8
Location: /home/odoo12/borg1/bees-test
Encrypted: Yes (repokey)
Cache: /home/odoo12/.cache/borg/7a42809ebe2bfe0dde03daafe5acc1c806170b9993dda2601cc529f941f109f8
Security dir: /home/odoo12/.config/borg/security/7a42809ebe2bfe0dde03daafe5acc1c806170b9993dda2601cc529f941f109f8
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
All archives:              334.82 GB            148.15 GB              7.58 GB
                       Unique chunks         Total chunks
Chunk index:                  108081              2156276
```
