#!/usr/bin/env python3

import pathlib
import subprocess
import sys
import time

BORGMATIC_BIN = pathlib.Path("~/.local/bin/borgmatic").expanduser()
BORGMATIC_CONFIG_DIR = pathlib.Path("~/.config/borgmatic.d/").expanduser()


def main():
    configs = list(BORGMATIC_CONFIG_DIR.glob("*.yaml"))
    processes = []
    result = 0
    for config in configs:
        # TODO: maybe (ab)use multiprocessing.pool.ThreadPool to prevent too many
        # processes from being run simultaneously.
        processes.append(
            subprocess.Popen(
                [
                    str(BORGMATIC_BIN),
                    "--verbosity",
                    "-1",
                    "--syslog-verbosity",
                    "1",
                    "-c",
                    str(config),
                ],
                stdout=sys.stdout,
                stderr=sys.stderr,
            )
        )
    for process in processes:
        process.wait()
        if process.returncode != 0:
            print("'{}' failed".format(" ".join(process.args)))
            result = 1
    return result


if __name__ == "__main__":
    sys.exit(main())
